package com.epam.rd.khranovskyi.web.boot.service.impl;

import com.epam.rd.khranovskyi.db.entity.Client;
import com.epam.rd.khranovskyi.web.boot.repository.ClientRepository;
import com.epam.rd.khranovskyi.web.boot.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public List<Client> getByName(String name) {
        return clientRepository.findClientByName(name);
    }

    @Override
    public List<Client> getAll() {
        return clientRepository.findAll();
    }

    @Override
    public Client getById(Long id) {
        return clientRepository.findById(id)
                .orElse(null);
    }
}
