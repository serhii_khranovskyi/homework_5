package com.epam.rd.khranovskyi.web.boot.repository;

import com.epam.rd.khranovskyi.db.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {


    List<Client> findClientByName(String name);
}